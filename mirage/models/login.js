import { Model, belongsTo } from 'ember-cli-mirage';

// Emulate a login from the API
export default Model.extend({
  user: belongsTo('user'),
});
