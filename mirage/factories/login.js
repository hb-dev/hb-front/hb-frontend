import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  token() {
    return faker.internet.password();
  },
});
