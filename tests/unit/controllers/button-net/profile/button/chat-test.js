import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | button-net/profile/button/chat', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:button-net/profile/button/chat');
    assert.ok(controller);
  });
});
