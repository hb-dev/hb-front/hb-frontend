import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { belongsToRelationship } from 'help-button/tests/helpers/model-helpers';

import { run } from '@ember/runloop';

module('Unit | Model | button', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    const model = run(() => this.owner.lookup('service:store').createRecord('button'));
    assert.ok(!!model);
  });

  belongsToRelationship('button', 'creator', {
    type: 'user',
    options: {
      inverse: 'buttons',
    },
  });

  belongsToRelationship('button', 'image', {
    options: {
      inverse: null,
    },
  });
});
