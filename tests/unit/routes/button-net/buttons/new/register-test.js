import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | button-net/buttons/new/register', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:button-net/buttons/new/register');
    assert.ok(route);
  });
});
