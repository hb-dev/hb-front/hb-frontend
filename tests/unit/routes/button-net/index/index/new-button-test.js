import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | button-net/index/index/new-button', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:button-net/index/index/new-button');
    assert.ok(route);
  });
});
