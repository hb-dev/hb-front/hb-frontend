import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | index/index/button/my-chat', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:index/index/button/my-chat');
    assert.ok(route);
  });
});
