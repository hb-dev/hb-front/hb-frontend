import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | user/buttons/chat', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:user/buttons/chat');
    assert.ok(route);
  });
});
