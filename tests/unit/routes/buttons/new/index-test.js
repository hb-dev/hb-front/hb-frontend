import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | buttons/new/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    const route = this.owner.lookup('route:buttons/new/index');
    assert.ok(route);
  });
});
