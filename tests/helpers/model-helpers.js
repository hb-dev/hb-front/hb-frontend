import { get } from '@ember/object';
import { assign } from '@ember/polyfills';
import { test } from 'qunit';

const DEFAULT_OPTIONS_FOR_RELATIONSHIPS = {
  async: true,
  polymorphic: false,
};

// TODO: Improve test messages
export function hasRelationship(modelName, relationship,
  { key = relationship, kind, type = relationship, optionsParam }) {
  test(`${modelName} has a relationship \`${relationship}\``, function(assert) {
    const store = this.store();
    const model = store.modelFor(modelName);
    const rel = get(model, 'relationshipsByName').get(relationship);
    const options = assign({}, DEFAULT_OPTIONS_FOR_RELATIONSHIPS, optionsParam);

    assert.ok(!!rel, `relationship ${relationship} exists in model ${modelName}`);

    assert.equal(rel.key, key,
      `expected to have \`${key}\` for relationship \`${relationship}\``);

    assert.equal(rel.kind, kind,
      `expected relationship \`${relationship}\` to be of kind \`${kind}\` but got \`${rel.kind}\``);

    assert.equal(rel.type, type,
      `expected relationship \`${relationship}\` to be of type \`${type}\` but got \`${rel.type}\``);

    if ('async' in options) {
      if (options.async) {
        assert.ok(rel.options.async || typeof rel.options.async === 'undefined',
          `expected relationship \`${relationship}\` to be async but it is not`);
      } else {
        assert.notOk(rel.options.async,
          `expected relationship \`${relationship}\` not to be async but it is`);
      }
    }

    if ('inverse' in options) {
      if (typeof options.inverse === 'string') {
        assert.equal(rel.options.inverse, options.inverse,
          `expected relationship \`${relationship}\` to be the inverse of
          \`${options.inverse}\` but got \`${rel.options.inverse}\``);
      } else if (!options.inverse) {
        assert.notOk(rel.options.inverse,
          `expected relationship \`${relationship}\` to have no inverse but it has
          \`${rel.options.inverse}\``);
      }
    }

    if ('polymorphic' in options) {
      if (options.polymorphic) {
        assert.ok(rel.options.polymorphic,
          `expected relationship \`${relationship}\` to be polymorphic but it is not`);
      } else {
        assert.notOk(rel.options.polymorphic,
          `expected relationship \`${relationship}\` not to be polymorphic but it is`);
      }
    }
  });
}

export function belongsToRelationship(modelName, relationship, options) {
  const newOptions = assign({}, options, { kind: 'belongsTo' });
  hasRelationship(modelName, relationship, newOptions);
}

export function hasManyRelationship(modelName, relationship, options) {
  const newOptions = assign({}, options, { kind: 'hasMany' });
  hasRelationship(modelName, relationship, newOptions);
}
