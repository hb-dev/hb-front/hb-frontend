// <Helpbuttons collaborative network making software>
//     Copyright (C) 2013-2121  Helpbuttons
//
//     This program is free software; you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation; either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License along
//     with this program; if not, write to the Free Software Foundation, Inc.,
//     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { computed, set } from '@ember/object';
import RSVP from 'rsvp';
import config from '../config/environment';
import $ from 'jquery';

export default Service.extend({
  ajax: service(),

  currentPosition: null,

  geolocationEnabled: computed(
    function() {
      return navigator.geolocation;
    }
  ),


  getCurrentPosition() {
    return new RSVP.Promise((resolve, reject)=>{
      const options = {
        enableHighAccuracy: true,
        timeout: 4000,
        maximumAge: 0,
      };
      const errorCallback = this.locationError(resolve, reject);
      navigator.geolocation.getCurrentPosition(this.locationSuccess(resolve), errorCallback, options);
    });
  },

  locationSuccess(resolve) {
    return (position) => {
      set(this, 'currentPosition', position.coords);
      resolve(position.coords);
    };
  },

  locationError(resolve, reject) {
    return ()=>{
      const { 'google-apis': {key}  } = config;

      $.ajax({
        type: 'POST',
        url: `https://www.googleapis.com/geolocation/v1/geolocate?key=${key}`,
      })
        .then((position)=>{
          const coordinates = {
            latitude: position.location.lat,
            longitude: position.location.lng,
          };
          resolve(coordinates);
        })
        .catch((reason)=>{
          const msg = 'Error: ' + reason.message + ' Code: ' + reason.code;
          const error = {
            message: msg,
          };
          reject(error);
        });
    };
  },
});
