import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed, get, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { notEmpty, empty, or } from '@ember/object/computed';
import { htmlSafe } from '@ember/string';
import { A } from '@ember/array';


export default Component.extend({
  classNames: ['button-new-date'],
  geolocation: service('geolocation'),
  screens: service('screen'),
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  isMon: computed('button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      return false;
    }
    return get(this, 'button.periodicDate').includes('Lunes');
  }),
  isTue: computed('button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      return false;
    }
    return get(this, 'button.periodicDate').includes('Martes');
  }),
  isWed: computed('button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      return false;
    }
    return get(this, 'button.periodicDate').includes('Miercoles');
  }),
  isThu: computed('button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      return false;
    }
    return get(this, 'button.periodicDate').includes('Jueves');
  }),
  isFri: computed('button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      return false;
    }
    return get(this, 'button.periodicDate').includes('Viernes');
  }),
  isSat: computed('button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      return false;
    }
    return get(this, 'button.periodicDate').includes('Sabado');
  }),
  isSun: computed('button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      return false;
    }
    return get(this, 'button.periodicDate').includes('Domingo');
  }),

  hasFormatedDate: notEmpty('formatedDate'),

  formatedDate: computed('button.date','button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      let date = get(this, 'button.date');
      const options = {
        weekday: 'long', month: 'long',
        day: 'numeric', hour: '2-digit', minute: '2-digit',
      };
      if (!isEmpty(date) && date !== 'Ahora') {
        date = new Date(date);
        date = date.toLocaleTimeString('es-es', options);
        return date.charAt(0).toUpperCase() + date.slice(1);
      }
      return 'Ahora';
    } else {
      let stringDate = get(this, 'button.periodicDate');
      if(!isEmpty(stringDate))
      {
          stringDate = stringDate.replace(/,([^,]*)$/, '$1');
          stringDate = stringDate.replace(/,([^,]*)$/, ' y ' + '$1');
          stringDate = stringDate.replace(/,/g, ", ");
      }
      return stringDate;
    }
  }),

  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      let bodyHeight = document.getElementsByClassName('button-new-date')[0];
      let contentHeight = document.getElementById('typeContent');
      bodyHeight.scrollTop = bodyHeight.scrollHeight;

      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.clientHeight;
        contentHeight = contentHeight.clientHeight;
        if (bodyHeight - contentHeight > 100) {
          const svgStyles = `height: ${bodyHeight - contentHeight}px;`;
          return htmlSafe(svgStyles);
        }
        const svgStyles = 'height: 100px;';
        return htmlSafe(svgStyles);
      }
      return '';
    }
  ),

  needsScrollbar: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      const containerHeight = document.getElementsByClassName('button-new-date')[0];
      const contentHeight = document.getElementsByClassName('button-new-date__body')[0];
      if (!isEmpty(containerHeight) && !isEmpty(contentHeight)) {
        return containerHeight.clientHeight > contentHeight.clientHeight ?
          '' : 'button-new-date__content-has-scroll';
      }
      return '';
    }),

  button: null,

  emptyPeriod: computed('button.periodicDate', function() {
    return isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}";
  }),

  buttonDateType: computed('button.date', function() {
    if (!isEmpty(this.button.periodicDate)) {
      return 'periodicDate';
    }
    if (this.button.date === 'Ahora') {
      return 'now';
    }
    return !isEmpty(this.button.date) ? 'specificDate' : null;
  }),

  showPicker: null,
  showPickerPeriod: null,


  selected: computed('button.date', function() {
    return !isEmpty(this.button.date) ? this.button.date : new Date();
  }),

  selectedPeriodic: computed('button.periodicDate', function() {
    return !isEmpty(this.button.periodicDate) ? this.button.periodicDate : new Date();
  }),

  bodyClass: computed('isSmallDevice', function() {
    return this.isSmallDevice ?
      'button-new-date__body' : 'button-new-date__body button-new-date__body--small-height';
  }),

  inputAddress: null,

  date: computed('button.date', function() {
    return !isEmpty(this.button.date) ? this.button.date : new Date();
  }),

  periodicDate: computed('button.periodicDate', function() {
    return !isEmpty(this.button.periodicDate) ? this.button.periodicDate : new Date();
  }),

  isNextButtonDisabled: empty('buttonDateType'),

  nextStepAction() {},

  buttonCardClick: false,

  actions: {
    nextStep() {
      if (this.buttonDateType === 'now') {
        set(this, 'button.date', 'Ahora');
      } else if (this.buttonDateType === 'specificDate') {
        set(this, 'button.date', this.selected);
      } else if (this.buttonDateType === 'periodicDate') {
        set(this, 'button.periodicDate', this.selectedPeriodic);
      }
      this.nextStepAction();
    },

    closeModal() {
      if(isEmpty(get(this,'button.date')))
      {
        set(this, 'buttonDateType', null);
      }
      set(this, 'showPicker', false);
      set(this, 'showPickerPeriod', false);
    },

    togglePicker(param) {
      set(this, 'button.date', null);
      this.toggleProperty('showPicker');
      set(this, 'buttonDateType', param);
    },

    togglePeriodPicker(param) {
      set(this, 'button.periodicDate', null);
      this.toggleProperty('showPickerPeriod');
      set(this, 'buttonDateType', param);
    },

    confirmDate() {
      const date = this.selected;
      date.setHours(this.date.getHours());
      date.setMinutes(this.date.getMinutes());
      set(this, 'button.date', date);
      this.send('closeModal');
      this.send('nextStep');
    },
    cardClick() {
      set(this, 'buttonCardClick', true);
    },
    addDay(param) {
      let periodicDate = get(this, 'button.periodicDate');
      if (isEmpty(periodicDate)) {
        periodicDate = param + ",";
      } else {
        if (periodicDate.includes(param)) {
          periodicDate = periodicDate.replace(param + ',', '');
        } else {
          periodicDate = periodicDate.concat(param + ',');
        }
      }
      set(this, 'button.periodicDate', periodicDate);
    },
    closeCreateModal() {
      if (!this.buttonCardClick) {
        this.closeAction();
      }
      set(this, 'buttonCardClick', false);
    },
    setRadioButton(value) {
      if (value !== 'periodicDate') {
        set(this, 'button.periodicDate', null);
      }
      set(this, 'buttonDateType', value);
    },
  },
});
