import Component from '@ember/component';
import { computed, set, get } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { notEmpty } from '@ember/object/computed';
import { inject as service } from '@ember/service';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  classNames: ['col-12 card-notification button-file__messages'],
  notifications: service('notification-messages'),
  chat: null,
  button: null,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },
  user: computed(
    'chat',
    {
      get() {
        this.chat.user.then((user) => {
          set(this, 'user', user);
        });
      },
      set(key, value) {
        return value;
      },
    }
  ),
  hasUserAvatar: notEmpty('userAvatar'),

  showNormalChat: true,

  hasUnreadMessages: computed('chat.unreadMessages', function() {
    get(this,'chat').reload();
    return get(this, 'chat.unreadMessages') > 0;
  }),

  userAvatar: computed(
    'user',
    {
      get() {
        if (!isEmpty(this.user)) {
          this.user.avatar.then((userAvatar) => {
            set(this, 'userAvatar', userAvatar);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),

  actions: {
    removeChat() {
      this.chat.deleteRecord();
      this.chat.save()
        .then(() => {
          this.notifications.success(
            'Chat eliminado correctamente',
            NOTIFICATION_OPTIONS,
          );
          this.toggleProperty('showConfirmation');
        })
        .catch(() => {
          this.notifications.success(
            'Error al eleminar el chat',
            NOTIFICATION_OPTIONS,
          );
        });
    },
    showConfirmationDialog() {
      this.toggleProperty('showConfirmation');
    },
  },
});
