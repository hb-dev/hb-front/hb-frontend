import Component from '@ember/component';
import { get, set, computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import { isEmpty } from '@ember/utils';
import MutableArray from '@ember/array/mutable';


const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};


export default Component.extend({
  store: service(),
  routing: service('-routing'),
  notifications: service('notification-messages'),
  session: service('session'),


  classNames: ['trending-tags'],

  init() {
    this._super(...arguments);
    if (this.router._router.url.includes('buttonsDisplay=list')) {
      set(this, 'isListView', 'list');
    }
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  isShowingMenuModal: null,
  searchTrendingTags: null,
  router: service(),
  isButtonNetRoute: false,
  buttonNet: null,
  isListView: null,

  togglePropertyMenu: {},

  globalTrendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'globalTrendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),


  netSelectedTags:  computed('buttonNet', function() {
    return get(this, 'buttonNet.tags');
  }),

  // sortedArray : computed('searchTrendingTags', function(a,b)  {
  //   return b[0] - a[0];
  // }),


  trendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let sortedArray = [];
    if(tags!=null)
    sortedArray = tags.sortBy('activeButtonsCounter').reverse();
    let tagsArr = [];
    if (!isEmpty(tags)) {
      for (let i = 0; i < sortedArray.length && i < 15; i++) {
        tagsArr.push({name: get(sortedArray[i], 'name'), activeButtonsCounter: get(sortedArray[i], 'activeButtonsCounter')});
      }
    }
    return tagsArr;
  }),

  actions: {

    create() {
       this.get('create')();
     },


     addInterest(param) {
       if(get(this, 'session.isAuthenticated')) {
         const currentUser = get(this, 'session.currentUser')
           get(currentUser, 'userTags').then((userTags) => {
               let alreadyExists = false;
               for (let j = 0; j < userTags.length; j++) {
                 if (userTags.objectAt(j).name === param.name) {
                   alreadyExists = true;
                 }
               }
               if (!alreadyExists) {
                 let tag = get(this, 'store').createRecord('userTag', {
                   name: param.name,
                   tag: param
                 });
                 tag.save().then((tagResp) => {
                   get(currentUser,'userTags').pushObject(tagResp);
                   currentUser.save();
                   this.notifications.success(
                     'Añadido #'+tagResp.name+' a tus intereses, puedes editarlo en tu perfil.',
                     NOTIFICATION_OPTIONS,
                   );
                 });
               } else {
                 this.notifications.error(
                   'Ya existe #'+param.name+' en tus intereses',
                   NOTIFICATION_OPTIONS,
                 );
               }
           })
       } else {
         const queryParams = {
           latitude: get(this, 'lastUserLat'),
           longitude: get(this, 'lastUserLng'),
           address: null,
           northEastLat: null,
           northEastLng: null,
           southWestLat: null,
           southWestLng: null,
           userMarker: true,
         };
         this.notifications.error(
           'Escribe tu mail para poder suscribirte a los temas',
           NOTIFICATION_OPTIONS,
         );
         this.transitionToRoute('register', {queryParams} );
       }
     },

    hideMenu () {
      this.togglePropertyMenu('isShowingMenuModal');
    },

  }

});
