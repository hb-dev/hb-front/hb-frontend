import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};


export default Component.extend({
  session: service('session'),
  routing: service('-routing'),
  notifications: service('notification-messages'),
  isAuthenticated: readOnly('session.isAuthenticated'),
  searchTrendingTags: null,
  indexController:null,

  isShowingMenuModal: null,
  isSmallDevice: null,
  hideWelcome: null,
  isShowingMoreOptions: false,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  newButtonRoute: computed('routing.currentRouteName', function() {
    if (get(this, 'isButtonNetRoute')) {
      if (!this.isSmallDevice) {
        return 'button-net.index.index.new-button';
      }
      return 'button-net.buttons.new';
    }
    if (!this.isSmallDevice) {
      return 'index.index.new-button';
    }
    return 'buttons.new';
  }),

  showSettings: false,

  currentUser: readOnly('session.currentUser'),

  currentUserAvatar: readOnly('currentUser.avatar'),

  isShowingMoreOptions: false,


  actions: {
    toggleProperty(param, hideWelcome) {
      this.toggleProperty(param);
      if (hideWelcome) {
        // set(this, 'hideWelcome', false);
      }
    },

    closeSession() {
      this.session.invalidate();
    },
    removeProfile() {
      window.alert('Not finished');
    },

    addInterest() {
      if(get(this, 'session.isAuthenticated')) {
        const currentUser = get(this, 'session.currentUser')
          get(currentUser, 'userTags').then((userTags) => {
            let t = get(this, 'firstTrendingTags');
            if(get(this, 'isButtonNetRoute')){
              t= get(this, 'buttonNet').tags;
            }
            get(this,'selectedTags').forEach((item, i) => {
              let alreadyExists = false;
              for (let j = 0; j < userTags.length; j++) {
                if (userTags.objectAt(j).name === item.name) {
                  alreadyExists = true
                }
              }
              if (!alreadyExists) {
                let tag = get(this, 'store').createRecord('userTag', {
                  name: item.name,
                  tag: item
                });
                tag.save().then((tagResp) => {
                  get(currentUser,'userTags').pushObject(tagResp);
                  currentUser.save();
                  this.notifications.success(
                    'Añadido #'+tagResp.name+' a tus intereses',
                    NOTIFICATION_OPTIONS,
                  );
                });
              } else {
                this.notifications.error(
                  'Ya existe #'+tagResp.name+' en tus intereses',
                  NOTIFICATION_OPTIONS,
                );
              }
            });
          })
      } else {
        const queryParams = {
          latitude: get(this, 'lastUserLat'),
          longitude: get(this, 'lastUserLng'),
          address: null,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: true,
        };
        this.notifications.error(
          'Escribe tu mail para poder suscribirte a los temas',
          NOTIFICATION_OPTIONS,
        );
        this.transitionToRoute('register', {queryParams} );
      }
    },


    toggleShowMoreOptions() {
      this.toggleProperty('isShowingMoreOptions');
    },

    rrssAuthenticate(providerName) {
      const context = this;
      this.session.authenticate('authenticator:torii', providerName).then(function() {
        if (context.routing.currentRouteName === 'index.index') {
          context.loginTransition();
        }
      }, context);
    },
  },
});
