export default function() {
  this.transition(
    this.hasClass('settings'),
    this.toValue(true),
    this.use('toLeft'),
    this.reverse('toRight'),
  );

  // Button Creation Steps MOBILE
  this.transition(
    this.fromRoute('buttons.new.index'),
    this.toRoute('buttons.new.description'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('buttons.new.description'),
    this.toRoute('buttons.new.location'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('buttons.new.location'),
    this.toRoute('buttons.new.date'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('buttons.new.date'),
    this.toRoute('buttons.new.register'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('buttons.new.register'),
    this.toRoute('buttons.new.login'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('buttons.new.register'),
    this.toRoute('buttons.new.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('buttons.new.login'),
    this.toRoute('buttons.new.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('buttons.new.date'),
    this.toRoute('buttons.new.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('buttons.new.activate-button'),
    this.toRoute('buttons.new.share'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  // Button Creation Steps DESKTOP
  this.transition(
    this.fromRoute('index.index.new-button.index'),
    this.toRoute('index.index.new-button.description'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index.index.new-button.description'),
    this.toRoute('index.index.new-button.location'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index.index.new-button.location'),
    this.toRoute('index.index.new-button.date'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index.index.new-button.date'),
    this.toRoute('index.index.new-button.register'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index.index.new-button.register'),
    this.toRoute('index.index.new-button.login'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index.index.new-button.register'),
    this.toRoute('index.index.new-button.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index.index.new-button.login'),
    this.toRoute('index.index.new-button.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index.index.new-button.date'),
    this.toRoute('index.index.new-button.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index.index.new-button.activate-button'),
    this.toRoute('index.index.new-button.share'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  // button-net transitions :


  this.transition(
    this.fromRoute('button-net.buttons.new.index'),
    this.toRoute('button-net.buttons.new.description'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.buttons.new.description'),
    this.toRoute('button-net.buttons.new.location'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.buttons.new.location'),
    this.toRoute('button-net.buttons.new.date'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.buttons.new.date'),
    this.toRoute('button-net.buttons.new.register'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.buttons.new.register'),
    this.toRoute('button-net.buttons.new.login'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.buttons.new.register'),
    this.toRoute('buttons.new.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.buttons.new.login'),
    this.toRoute('buttons.new.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.buttons.new.date'),
    this.toRoute('buttons.new.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.buttons.new.activate-button'),
    this.toRoute('buttons.new.share'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  // Button Creation Steps DESKTOP
  this.transition(
    this.fromRoute('button-net.index.index.new-button.index'),
    this.toRoute('button-net.index.index.new-button.description'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.index.index.new-button.description'),
    this.toRoute('button-net.index.index.new-button.location'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.index.index.new-button.location'),
    this.toRoute('button-net.index.index.new-button.date'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.index.index.new-button.date'),
    this.toRoute('button-net.index.index.new-button.register'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.index.index.new-button.register'),
    this.toRoute('button-net.index.index.new-button.login'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.index.index.new-button.register'),
    this.toRoute('button-net.index.index.new-button.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.index.index.new-button.login'),
    this.toRoute('button-net.index.index.new-button.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.index.index.new-button.date'),
    this.toRoute('button-net.index.index.new-button.activate-button'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('button-net.index.index.new-button.activate-button'),
    this.toRoute('button-net.index.index.new-button.share'),
    this.use('toLeft'),
    this.reverse('toRight')
  );
}
