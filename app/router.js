// <Helpbuttons collaborative network making software>
//     Copyright (C) 2013-2121  Helpbuttons
//
//     This program is free software; you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation; either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License along
//     with this program; if not, write to the Free Software Foundation, Inc.,
//     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL,
});

Router.map(function() {
  this.route('login');
  this.route('dashboard');
  this.route('signup');

  this.route('buttons', function() {
    this.route('button', { path: 'button/:button_id'}, function() {
      this.route('my-chat', { path: 'chat/:chat_id'});
      this.route('chat', { path: 'chat'});
    });
    this.route('new', { path: '/:button_id'}, function() {
      this.route('description');
      this.route('location');
      this.route('activate-button');
      this.route('share');
      this.route('login');
      this.route('register');
      this.route('date');
    });
  });

  this.route('profile', function() {
    this.route('button', { path: 'button/:button_id'}, function() {
      this.route('my-chat', { path: 'chat/:chat_id'});
      this.route('chat', { path: 'chat'});
    });
    this.route('new-button', { path: '/:button_id'}, function() {
      this.route('description');
      this.route('location');
      this.route('activate-button');
      this.route('login');
      this.route('register');
      this.route('share');
      this.route('date');
    });
  });
  this.route('components-repo');
  this.route('register');
  this.route('recovery');
  this.route('index', { path: '' }, function() {
    this.route('index', { path: '' }, function() {
      this.route('button', { path: 'button/:button_id'}, function() {
        this.route('chat', { path: 'chat'});
        this.route('login');
        this.route('register');
        this.route('my-chat', { path: 'chat/:chat_id'});
      });
      this.route('new-button', { path: '/:button_id'}, function() {
        this.route('description');
        this.route('location');
        this.route('activate-button');
        this.route('login');
        this.route('register');
        this.route('share');
        this.route('date');
      });

      this.route('new', function() {});
    });
  });
  this.route('user', { path: 'user/:user_id'}, function() {
    this.route('buttons', function() {
      this.route('chat', { path: 'chat/:chat_id'});
    });
  });
  this.route('profile-edition');
  this.route('others');
  this.route('button-net', { path: '/:button_net_name'}, function() {
    this.route('login');
    this.route('others');
    this.route('profile-edition');
    this.route('buttons', function() {
      this.route('button', { path: 'button/:button_id'}, function() {
        this.route('my-chat', { path: 'chat/:chat_id'});
        this.route('chat', { path: 'chat'});
      });
      this.route('new', { path: '/:button_id'}, function() {
        this.route('description');
        this.route('location');
        this.route('activate-button');
        this.route('share');
        this.route('login');
        this.route('register');
        this.route('date');
      });
    });
    this.route('profile', function() {
      this.route('button', { path: 'button/:button_id'}, function() {
        this.route('my-chat', { path: 'chat/:chat_id'});
        this.route('chat', { path: 'chat'});
      });
      this.route('new-button', { path: '/:button_id'}, function() {
        this.route('description');
        this.route('location');
        this.route('activate-button');
        this.route('login');
        this.route('register');
        this.route('share');
        this.route('date');
      });
    });
    this.route('recovery');
    this.route('register');
    this.route('user', { path: 'user/:user_id'}, function() {
      this.route('buttons', function() {
        this.route('chat');
      });
    });
    this.route('index', { path: '' }, function() {
      this.route('index', { path: '' }, function() {
        this.route('button', { path: 'button/:button_id'}, function() {
          this.route('chat', { path: 'chat'});
          this.route('login');
          this.route('register');
          this.route('my-chat', { path: 'chat/:chat_id'});
        });
        this.route('new-button', { path: '/:button_id'}, function() {
          this.route('description');
          this.route('location');
          this.route('activate-button');
          this.route('login');
          this.route('register');
          this.route('share');
          this.route('date');
        });

        this.route('new', function() {});
        //
        // this.route('new', function() {});
      });
    });
  });

  this.route('404', { path: '/*path' });
  this.route('error');
});


export default Router;
