import Controller from '@ember/controller';
import { set, get, computed } from '@ember/object';


export default Controller.extend({
  button: null,

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),

  actions: {
      closeAction() {
        this.transitionToRoute('profile');
      },
      nextStep(){
        this.transitionToRoute("profile.new-button.activate-button");
      }
    },
  });
