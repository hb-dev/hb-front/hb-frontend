import Controller from '@ember/controller';
import { alias } from '@ember/object/computed';
import ActivateStepMixin from '../../../mixins/activate-step-mixin';
import { set, get } from '@ember/object';


export default Controller.extend(ActivateStepMixin,{
  button: alias('model'),
  loading: false,

  init() {
    this._super(...arguments);
    set(this, 'isNew', window.location.href.includes('new'));
  },

  isNew:null,

  actions: {
    closeAction() {
      this.transitionToRoute('profile');
    },
  },

  activationRoute: 'profile.new-button.share',

});
