import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { or } from '@ember/object/computed';
import { computed, get } from '@ember/object';

window.addEventListener('resize', () => {
  // let vh = window.innerHeight * 0.01;
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

export default Controller.extend({
  routing: service('-routing'),
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  isMapRoute: computed(
    'routing.currentRouteName',
    'routing.router.currentURL',
    function() {
      if (get(this, 'routing.currentRouteName').startsWith('index.index') || get(this, 'routing.currentRouteName').startsWith('button-net.index.index')) {
        return this.routing.router.currentURL.includes('buttonsDisplay=map')
        || !this.routing.router.currentURL.includes('buttonsDisplay=');
      }
      return false;
    }
  ),

  isMobileButtonCreationRoute: computed('routing.currentRouteName', function() {
    return get(this, 'routing.currentRouteName').startsWith('buttons.new');
  }),

  isModalOpened: computed('routing.currentRouteName', function() {
    switch (this.routing.currentRouteName) {
      case 'index.index.button.index':
        document.body.className = 'ember-application overflow-hidden';
        return true;
      case 'index.index.button.chat':
        document.body.className = 'ember-application overflow-hidden';
        return true;
      default:
        if (this.routing.currentRouteName.includes('index.index.new-button')) {
          document.body.className = 'ember-application overflow-hidden';
          return true;
        }
        document.body.className = 'ember-application';
        return false;
    }
  }),

  hideFooter: or('isSmallDevice', 'isMapRoute', 'isModalOpened'),
});
