import Controller from '@ember/controller';
import { alias } from '@ember/object/computed';
import ActivateStepMixin from '../../../mixins/activate-step-mixin';
import { set, get } from '@ember/object';


export default Controller.extend(ActivateStepMixin,{

  init() {
    this._super(...arguments);
    set(this, 'isNew', window.location.href.includes('new'));
  },

  isNew:null,

  button: alias('model'),
  loading: false,
  activationRoute: 'buttons.new.share',
});
