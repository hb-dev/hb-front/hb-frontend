import Controller from '@ember/controller';
import { set, get, computed } from '@ember/object';


export default Controller.extend({
  button: null,

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),

  actions: {
      closeAction() {
        this.transitionToRoute('index.index');
      },
      nextStep(){
        this.transitionToRoute("index.index.new-button.activate-button");
      }
    },
  });
