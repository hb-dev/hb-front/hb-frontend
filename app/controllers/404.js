import Controller from '@ember/controller';

export default Controller.extend({
  isShowingModal: false,

  actions: {
    searchByAddress(address, tags) {
      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
        searchTags: tags,
      };
      this.transitionToRoute('index.index', { queryParams });
    },
  }
});
