import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { get, set, getProperties } from '@ember/object';
import { notEmpty, or } from '@ember/object/computed';
import { bind } from '@ember/runloop';
import $ from 'jquery';
import config from '../config/environment';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Controller.extend({
  mediaQueries: service(),
  notifications: service('notification-messages'),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  email: null,
  password: null,
  passwordConfirmation: null,
  errorMessages: null,
  queryParams: ['t'],
  t: null,

  hasError: notEmpty('errorMessages'),

  recover() {
    this.notifications.success(
      `Se te ha enviado un correo a: ${this.email}`,
      NOTIFICATION_OPTIONS
    );

    this.transitionToRoute('index');
  },

  reset() {
    this.notifications
      .success(
        'Se ha restablecido tu contraseña con éxito!',
        NOTIFICATION_OPTIONS
      );
    set(this, 'queryParams.t', null);
    this.transitionToRoute('login');
  },

  actions: {
    recoverPassword() {
      const context = this;

      return $.ajax({
        type: 'POST',
        url: `${config.namespace}/recovery`,
        contentType: 'application/vnd.api+json',

        data: JSON.stringify({ email: this.email }),
      })
        .done(
          bind(context, get(context, 'recover'))
        )
        .fail(({ responseJSON }) => set(this, 'errorMessages', responseJSON.errors));
    },
    resetPassword() {
      const context = this;
      const {
        t: token,
        password,
        passwordConfirmation,
      } = getProperties(this, 't', 'password',
        'passwordConfirmation');

      const dataReceived = {
        password,
        'password-confirmation': passwordConfirmation,
      };

      return $.ajax({
        type: 'PATCH',
        url: `${config.namespace}/password/reset/${token}`,
        contentType: 'application/vnd.api+json',
        data: JSON.stringify(dataReceived),
      })
        .done(() => {
          bind(context, get(context, 'reset'))
        }
        )
        .fail(({ responseJSON }) => {
          if (responseJSON.errors[0].detail.includes('too_short')) {
            this.notifications.error(
              `Contraseña demasiado corta`,
              NOTIFICATION_OPTIONS
            );
          } else if (responseJSON.errors[0].detail.includes('password_confirmation')) {
            this.notifications.error(
              `Contraseña no coincide`,
              NOTIFICATION_OPTIONS
            );
          } else if (responseJSON.errors[0].detail.includes('reset_password_token.invalid')) {
            this.notifications.error(
              `El enlace ha caducado o no es válido`,
              NOTIFICATION_OPTIONS
            );
          } else {
            this.notifications.error(
              `Error al reestablecer la contraseña`,
              NOTIFICATION_OPTIONS
            );
          }
          set(this, 'errorMessages', responseJSON.errors)
        });
    },
    searchByAddress(address) {
      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
      };
      return this.transitionToRoute('index', { queryParams });
    },
    backAction() {
      window.history.back();
    },
    closeAction() {
      this.transitionToRoute('index.index');
    },
  },
});
