import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { get, set, getProperties } from '@ember/object';
import { notEmpty, or } from '@ember/object/computed';
import { bind } from '@ember/runloop';
import $ from 'jquery';
// import config from ' ../../config/environment';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Controller.extend({
  mediaQueries: service(),
  notifications: service('notification-messages'),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  email: null,
  password: null,
  passwordConfirmation: null,
  errorMessages: null,
  buttonNet: null,
  queryParams: ['t'],
  t: null,

  hasError: notEmpty('errorMessages'),

  recover() {
    this.notifications.success(
      `Se te ha enviado un correo a: ${this.email}`,
      NOTIFICATION_OPTIONS
    );

    this.transitionToRoute('button-net.index');
  },

  reset() {
    this.notifications
      .success(
        'Se ha restablecido tu contraseña con éxito!',
        NOTIFICATION_OPTIONS
      );
    this.transitionToRoute('button-net.login');
  },

  actions: {
    recoverPassword() {
      const context = this;

      return $.ajax({
        type: 'POST',
        url: `/api/v1/recovery`,
        contentType: 'application/vnd.api+json',

        data: JSON.stringify({ email: this.email }),
      })
        .done(
          bind(context, get(context, 'recover'))
        )
        .fail(({ responseJSON }) => set(this, 'errorMessages', responseJSON.errors));
    },
    resetPassword() {
      const context = this;
      const {
        t: token,
        password,
        passwordConfirmation,
      } = getProperties(this, 't', 'password',
        'passwordConfirmation');

      const dataReceived = {
        password,
        'password-confirmation': passwordConfirmation,
      };

      return $.ajax({
        type: 'PATCH',
        url: `/api/v1/password/reset/${token}`,
        contentType: 'application/vnd.api+json',
        data: JSON.stringify(dataReceived),
      })
        .done(
          bind(context, get(context, 'reset'))
        )
        .fail(({ responseJSON }) => set(this, 'errorMessages', responseJSON.errors));
    },
    searchByAddress(address) {
      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
      };
      return this.transitionToRoute('button-net.index', { queryParams });
    },
    backAction() {
      window.history.back();
    },
    closeAction() {
      this.transitionToRoute('button-net.index.index');
    },
  },
});
