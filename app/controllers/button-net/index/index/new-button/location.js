import Controller from '@ember/controller';
import { set, get, computed } from '@ember/object';


export default Controller.extend({
  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),

  actions: {
    nextStep() {
      this.transitionToRoute('button-net.index.index.new-button.date');
    },
    closeAction() {
      this.transitionToRoute('button-net.index.index');
    },
  },
});
