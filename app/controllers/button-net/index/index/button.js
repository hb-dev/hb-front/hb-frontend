import Controller from '@ember/controller';
import { readOnly } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
  routing: service('-routing'),
  session: service(''),
  button: readOnly('model.button'),
  chats: readOnly('model.chats'),
  accessDenied: '',

  buttonFileClass: computed('routing.currentPath', function() {
    if (this.routing.currentPath === 'button-net.index.index.button.chat') {
      return 'button-file__modal button-file__modal--top-position';
    }
    return 'button-file__modal';
  }),
  actions: {
    closeAction() {
      this.transitionToRoute('button-net.index.index');
    },
    openConversation() {
      // if (!get(this, 'session.isAuthenticated')) {
      //   this.transitionToRoute('button-net.index.index.button.register');
      // } else {
        this.transitionToRoute('chat', { queryParams: { id: this.button.id, isButton: true }});
      // }
    },
  },
});
