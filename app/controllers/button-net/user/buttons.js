import Controller, { inject as controller } from '@ember/controller';
import { get, set, computed } from '@ember/object';
import { readOnly, or } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import ButtonsFiltersMixin from '../../../mixins/buttons-filters-mixin';
import { htmlSafe } from '@ember/string';
import { resolve } from 'rsvp';



const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};


export default Controller.extend(
  ButtonsFiltersMixin,
  {
    mediaQueries: service(),
    session: service(),
    routing: service('-routing'),
    notifications: service('notification-messages'),
    store: service(),

    buttonNet: null,
    // QUERY PARAMS
    queryParams: [
      'searchTags',
    ],

    user: readOnly('model'),

    showProfile: false,
    resetFilters: false,
    socialShareNetwork: null,
    socialShareURL: null,
    // accessDenied: '',
    currentUser: readOnly('session.currentUser'),
    isAuthenticated: readOnly('session.isAuthenticated'),
    liked:null,

    accessDenied: computed('currentUser', function() {
      if (isEmpty(this.session.currentUser)) {
        set(this, 'accessDenied', '');
      } else {
        if (isEmpty(this.session.currentUser.internalId)) {
          get(this.session, 'currentUser').then((currentUser) => {

            if (get(currentUser, 'blockedBy').includes(Number(get(this, 'user.id')))) {
              set(this, 'accessDenied', 'disabled');
            } else {
              set(this, 'accessDenied', '');
            }
          });
        } else {
          if (get(this.session.currentUser, 'blockedBy').includes(Number(get(this, 'user.id')))) {
            // set(this, 'accessDenied', 'disabled');
            return 'disabled';
          }
          return '';
        }
      }
    }),

    userBlocked: computed(
      'currentUser.blocked',
      'currentUser.blocked.length',
      'currentUser.blocked.[]',
      'currentUser.blocked.@each',
      'changedUsersToggle',
      {
        get() {
          if (!isEmpty(get(this, 'currentUser.blocked'))) {
            for (let i = 0; i < get(this, 'currentUser.blocked').length; i++) {
              if (!isEmpty(get(this, 'currentUser.blocked')[i])) {
                if(this.user.id == get(this, 'currentUser.blocked')[i]) {
                  return true;
                }
              }
            }
          }
          return false;
        },
        set(key, value) { return value; },
      }
    ),

    userLiked: computed(
      'currentUser.liked',
      'currentUser.liked.length',
      'currentUser.liked.[]',
      'currentUser.liked.@each',
      'changedUsersToggle',
      {
        get() {
          if (!isEmpty(get(this, 'currentUser.liked'))) {
              for (let i = 0; i < get(this, 'currentUser.liked').length; i++) {
                if (!isEmpty(get(this, 'currentUser.liked')[i])) {
                if(this.user.id == get(this, 'currentUser.liked')[i]) {
                  return true;
                }
              }
            }
          }
          return false;
        },
        set(key, value) { return value; },
      }
    ),

    userBlockedMyNet: computed(
      'buttonNetController.model.blockedUsers',
      'buttonNetController.model.blockedUsers.length',
      'buttonNetController.model.blockedUsers.[]',
      'buttonNetController.model.blockedUsers.@each',
      'changedUsersToggle',
      {
        get() {
          const array = [];
          const bnet = get(this, 'buttonNet');
          if (get(this, 'buttonNetController.model.blockedUsers').includes(get(this, 'user.id')) || get(this, 'buttonNetController.model.blockedUsers').includes(Number(get(this, 'user.id'))))
          {
            return true;
          }
          else {
            return false;
          }
        },
        set(key, value) { return value; },
      }
    ),

    isMyNet: readOnly('buttonNetController.isCurrentNetMine'),

    userAdminMyNet: computed(
      'buttonNetController.model.adminUsers',
      'buttonNetController.model.adminUsers.length',
      'buttonNetController.model.adminUsers.[]',
      'buttonNetController.model.adminUsers.@each',
      'changedUsersToggle',
      {
        get() {
          const array = [];
          const bnet = get(this, 'buttonNet');
          if (get(this, 'buttonNetController.model.adminUsers').includes(get(this, 'user.id')) || get(this, 'buttonNetController.model.adminUsers').includes(Number(get(this, 'user.id'))))
          {
            return true;
          }
          else {
            return false;
          }
        },
        set(key, value) { return value; },
      }
    ),

    showChatRoute: false,
    showPulsedButtons: false,
    buttons: computed(
      'showPulsedButtons',
      'user',
      {
        get() {
          const relationshipName = this.showPulsedButtons ? 'buttons' : 'ownedButtons';
          const user = this.user;

          get(user, relationshipName)
            .then((buttons) => {
              set(this, 'buttons', buttons);
            });
        },
        set(key, value) { return value; },
      }
    ),

    buttonsFiltered: null,

    buttonNetController: controller('button-net'),

    isCurrentNetMine: readOnly('buttonNetController.isCurrentNetMine'),

    isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

    selectedButton: null,
    hasSelectedButton: computed('selectedButton', function() {
      if (!isEmpty(this.selectedButton)) {
        document.body.className = 'ember-application overflow-hidden';
        return true;
      }
      document.body.className = 'ember-application';
      return false;
    }),

    //add top padding to __content
    asideBodyStyles: computed(
      'selectedTags.[]',
      'isSmallDevice',
      'buttonsDisplay',
      function() {
        get(this, 'tagsToApply');
        const issmall = get(this, 'isSmallDevice');
        const headerHeight = document.getElementsByClassName('component-header')[0].clientHeight;
        const searchTopBarHeight = document.getElementsByClassName('search-topbar')[0].clientHeight;
        var filtersHeight = 0;
        var pulsedCreatedHeight = 0;
        var bodyMinHeight = 0;
        if(!this.isSmallDevice)
        {
          filtersHeight =  document.getElementById('filters').clientHeight;
         // pulsedCreatedHeight = document.getElementById('pulsedCreated').clientHeight;
         bodyMinHeight=1500;
        }
        const headerSectionHeight = headerHeight + searchTopBarHeight + filtersHeight + pulsedCreatedHeight ;
        const asideStyles = `padding-top: ${headerSectionHeight-60}px; min-height: ${bodyMinHeight}px`;
        return htmlSafe(asideStyles);
      }
    ),


    actions: {
      // searchByAddress(address) {
      //   const queryParams = {
      //     latitude: null,
      //     longitude: null,
      //     address,
      //     northEastLat: null,
      //     northEastLng: null,
      //     southWestLat: null,
      //     southWestLng: null,
      //     userMarker: null,
      //   };
      //   return this.transitionToRoute('button-net.index', { queryParams });
      // },

      searchByAddress(address, tag) {
        const urlParams = new URLSearchParams(window.location.search);
        let newTags = "";
        if (!isEmpty(urlParams.get('searchTags'))) {
          newTags = 'searchTags=' + urlParams.get('searchTags') + '&';
        }
        if (!isEmpty(tag)) {
          newTags += tag;
        }

        const queryParams = {
          latitude: null,
          longitude: null,
          address,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: null,
          searchTags: newTags,
        };
        return resolve().then(() => {
          this.transitionToRoute('button-net.user.buttons', { queryParams });
        });
      },

      setPulsedButtonsFilter(value) {
        set(this, 'showPulsedButtons', value);
      },

      selectButton(selectedButton) {
        set(this, 'selectedButton', selectedButton);
      },

      openConversation() {
        set(this, 'showChatRoute', true);
        this.transitionToRoute('button-net.user.buttons.chat', this.selectedButton.id,
          { queryParams: { id: this.selectedButton.id, isButton: true }});
      },

      closeAction() {
        set(this, 'selectedButton', null);
        set(this, 'showProfile', null);
      },
      goHome() {
        this.send('closeAction');
        this.transitionToRoute('button-net.profile.index');
      },
      socialShareTransition() {
        switch (this.socialShareNetwork) {
          case 'facebook':
            window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.socialShareURL, '_blank');
            break;
          case 'twitter':
            window.open('https://twitter.com/share?url=' + this.socialShareURL, '_blank');
            break;
          case 'linkedin':
            window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.socialShareURL, '_blank');
            break;
          case 'whatsapp':
            window.open('whatsapp://send?text=' + this.socialShareURL, '_blank');
            break;
          default:
        }
      },
      toggleProfileState() {
        this.toggleProperty('currentUser.active');
        return this.currentUser.save().then((user) => {
          if (user.active) {
            this.notifications.success(
              'Has activado tu usuario',
              NOTIFICATION_OPTIONS,
            );
          } else {
            this.notifications.info(
              'Has desactivado tu usuario. Tus botones no serán visibles para el resto de usuarios.',
              NOTIFICATION_OPTIONS,
            );
          }
        });
      },
      like() {
        set(this, 'currentUser.actionEvent', 'like');
        set(this, 'currentUser.otherUserId', get(this, 'user.id'));
        get(this, 'currentUser').save()
          .then((currentUser) => {
            if (currentUser.liked.includes(Number(get(this, 'user.id')))) {
              this.notifications.success(
                'Has apoyado a este usuario!',
                NOTIFICATION_OPTIONS,
              );
            } else {
              this.notifications.success(
                'Has dejado de apoyar a este usuario!',
                NOTIFICATION_OPTIONS,
              );
            }
          })
          .catch(() => {
            this.notifications.error(
              'Ya diste apoyo a este usuario',
              NOTIFICATION_OPTIONS,
            );
          });
      },
      adminNet() {
        let alreadyAdded = false;
        if (get(this, 'buttonNetController.model.adminUsers').includes(get(this, 'user.id')) || get(this, 'buttonNetController.model.adminUsers').includes(Number(get(this, 'user.id')))) {
          let adminUsers = get(this, 'buttonNetController.model.adminUsers');
          if (adminUsers.indexOf(get(this, 'user.id')) > -1) {
            adminUsers.splice(adminUsers.indexOf(get(this, 'user.id')), 1);
          } else {
            adminUsers.splice(adminUsers.indexOf(Number(get(this, 'user.id'))), 1);
          }
          alreadyAdded = true;
          set(this, 'buttonNetController.model.adminUsers', adminUsers);
        } else {
          get(this, 'buttonNetController.model.adminUsers').push(get(this, 'user.id'));
        }
        get(this, 'buttonNetController.model').save().then((user) => {
          if (alreadyAdded) {
            this.notifications.success(
              'Administrador borrado',
              NOTIFICATION_OPTIONS,
            );
          } else {
            this.notifications.success(
              'Administrador guardado',
              NOTIFICATION_OPTIONS,
            );
          }
        })
        .catch((error) => {
          // this.notifications.error(
          //   'Ya bloqueaste a este usuario',
          //   NOTIFICATION_OPTIONS,
          // );
          this.notifications.error(
            'Error en el convertir en administrador a este usuario',
            NOTIFICATION_OPTIONS,
          );
        });
      },
      block() {
        set(this, 'currentUser.actionEvent', 'block');
        set(this, 'currentUser.otherUserId', get(this, 'user.id'));
        get(this, 'currentUser').save()
          .then(() => {
            if (currentUser.blocked.includes(Number(get(this, 'user.id')))) {
              this.notifications.success(
                'Has bloqueado a este usuario, ya no podrá pulsar tus botones.',
                NOTIFICATION_OPTIONS,
              );
            } else {
              this.notifications.success(
                'Has desbloqueado a este usuario, ya podrá volver a pulsar tus botones.',
                NOTIFICATION_OPTIONS,
              );
            }
          })
          .catch(() => {
            this.notifications.error(
              'Ya bloqueaste a este usuario',
              NOTIFICATION_OPTIONS,
            );
          });
      },

      blockNet() {
        let alreadyAdded = false;
        if (get(this, 'buttonNetController.model.blockedUsers').includes(get(this, 'user.id')) || get(this, 'buttonNetController.model.blockedUsers').includes(Number(get(this, 'user.id')))) {
          let blockedUsers = get(this, 'buttonNetController.model.blockedUsers');
          if (blockedUsers.indexOf(get(this, 'user.id')) > -1) {
            blockedUsers.splice(blockedUsers.indexOf(get(this, 'user.id')), 1);
          } else {
            blockedUsers.splice(blockedUsers.indexOf(Number(get(this, 'user.id'))), 1);
          }
          alreadyAdded = true;
          set(this, 'buttonNetController.model.blockedUsers', blockedUsers);
        } else {
          get(this, 'buttonNetController.model.blockedUsers').push(get(this, 'user.id'));
        }
        get(this, 'buttonNetController.model').save().then((user) => {
          if (alreadyAdded) {
            this.notifications.success(
              'Has desbloqueado a este usuario en esta red',
              NOTIFICATION_OPTIONS,
            );
          } else {
            this.notifications.success(
              'Has bloqueado a este usuario en esta red',
              NOTIFICATION_OPTIONS,
            );
          }
        })
        .catch((error) => {
          // this.notifications.error(
          //   'Ya bloqueaste a este usuario',
          //   NOTIFICATION_OPTIONS,
          // );
          this.notifications.error(
            'Error en el bloqueo de este usuario',
            NOTIFICATION_OPTIONS,
          );
        });
      }
    },
  }
);
