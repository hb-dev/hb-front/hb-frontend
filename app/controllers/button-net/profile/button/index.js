import Controller from '@ember/controller';
import { readOnly } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { get,set } from '@ember/object'

export default Controller.extend({
  session: service(''),
  button: readOnly('model.button'),
  chats: readOnly('model.chats'),

  selectedButton: null,
  socialShareNetwork: null,
  socialShareURL: null,

  actions: {
    closeAction() {
      this.transitionToRoute('button-net.profile');
    },
    openConversation() {
      this.transitionToRoute('button-net.profile.button.chat', { queryParams: {id: this.button.id, isButton: true }});
    },
    socialShareTransition() {
      switch (this.socialShareNetwork) {
        case 'facebook':
          window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.socialShareURL, '_blank');
          break;
        case 'twitter':
          window.open('https://twitter.com/share?url=' + this.socialShareURL, '_blank');
          break;
        case 'linkedin':
          window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.socialShareURL, '_blank');
          break;
        case 'whatsapp':
          window.open('whatsapp://send?text=' + this.socialShareURL, '_blank');
          break;
        default:
      }
    },
    removeButtonTransition() {
      set(this, 'selectedButton', null);
      this.transitionToRoute('button-net.profile');
    },
  },
});
