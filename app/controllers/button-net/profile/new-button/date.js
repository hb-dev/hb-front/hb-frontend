import Controller from '@ember/controller';
import { set, get, computed } from '@ember/object';


export default Controller.extend({
  button: null,
  buttonNetController: controller('button-net'),

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),

  actions: {
      closeAction() {
        this.transitionToRoute('button-net.profile');
      },
      nextStep(){
        this.transitionToRoute("button-net.profile.new-button.activate-button");
      }
    },
  });
