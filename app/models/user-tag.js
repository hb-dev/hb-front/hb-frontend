// <Helpbuttons collaborative network making software>
//     Copyright (C) 2013-2121  Helpbuttons
//
//     This program is free software; you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation; either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License along
//     with this program; if not, write to the Free Software Foundation, Inc.,
//     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed, set, get } from '@ember/object';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { isEmpty } from '@ember/utils'


export default Model.extend({
  name: computed( 'tag', {
    get() {
      get(this, 'tag').then((tagResp) => {
        !isEmpty(tagResp) ? set(this, 'name', tagResp.name) : set(this, 'name', '');
      })
    },
    set(key, value) {
      return value;
    },
  }),
  activeButtonsCounter: computed( 'tag', {
    get() {
      get(this, 'tag').then((tagResp) => {
        !isEmpty(tagResp) ? set(this, 'activeButtonsCounter', tagResp.activeButtonsCounter) : set(this, 'activeButtonsCounter', '');
      })
    },
    set(key, value) {
      return value;
    },
  }),
  tagType: attr('number', { defaultValue: 2 }),

  user: belongsTo('user'),
  tag: belongsTo('tag'),

  needType: computed( 'tagType', function() {
    if (this.tagType==0 || this.tagType==3) {
      return false;
    }
    return true;
  }),

  offerType: computed( 'tagType', function() {
    if (this.tagType==1 || this.tagType==3) {
      return false;
    }
    return true;
  }),

  setType: computed( 'needType','offerType', {
    get() {
      set(this,'hasDirtyAttributes', true);
      if (this.needType && this.offerType) {
        set(this,'tagType',2);
      } else {
        if (this.needType) {
          set(this,'tagType',1);
        }
        if (this.offerType) {
          set(this,'tagType',0);
        }
        if (!this.needType && !this.offerType) {
          set(this,'tagType',3);
        }
      }
      return null;
    },
    set(key, value) {
      return value;
    }
  }),

  buttonNets: attr(),
});
