// <Helpbuttons collaborative network making software>
//     Copyright (C) 2013-2121  Helpbuttons
//
//     This program is free software; you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation; either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License along
//     with this program; if not, write to the Free Software Foundation, Inc.,
//     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Model.extend({
  lastMessageDate: attr('date'),
  user: belongsTo('user'),
  messages: hasMany('message'),
  button: belongsTo('button'),
  buttonCreator: belongsTo('user'),
  unreadMessages: attr('number'),

  lastMessageDateFormated: computed('lastMessageDate', function() {
    if (!isEmpty(this.lastMessageDate)) {
      const oneDay = 24 * 60 * 60 * 1000;
      const firstDate = new Date();
      const secondDate = this.lastMessageDate;

      const diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
      if (diffDays === 0) {
        return 'hoy';
      } else if (diffDays === 1) {
        return '1 día';
      }
      return diffDays + 'días';
    }
    return 'No hay mensajes';
  }),
});
