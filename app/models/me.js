// <Helpbuttons collaborative network making software>
//     Copyright (C) 2013-2121  Helpbuttons
//
//     This program is free software; you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation; either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License along
//     with this program; if not, write to the Free Software Foundation, Inc.,
//     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
  email: attr('string'),
  name: attr('string'),
  showPhone: attr('boolean', { defaultValue: false }),
  useExternalConv: attr('boolean', { defaultValue: false }),
  useTelegram: attr('boolean', { defaultValue: false }),
  useWhatsapp: attr('boolean', { defaultValue: false }),
  userTelegram: attr('boolean', { defaultValue: false }),
  deliverInterests: attr('boolean', { defaultValue: false }),
  likes: attr('number', { defaultValue: 0 }),
  liked: attr(),
  blocked: attr(),
  blockedBy: attr(),
  actionEvent: attr(),

  admin: attr('boolean'),
  superAdmin: attr('boolean'),

  // Attribute when changing password
  password: attr('string'),
  passwordConfirmation: attr('string'),

  avatar: belongsTo('image', { inverse: null }),

  buttons: hasMany('button', { inverse: 'creator' }),

  chats: hasMany('chat', { inverse: null }),
});
