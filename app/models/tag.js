import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed, set } from '@ember/object';


export default Model.extend({
  name: attr('string'),
  activeButtonsCounter: attr('number', { defaultValue: 0 }),
});
