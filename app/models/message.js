import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';
import currentUser from '../utils/current-user';
import { readOnly } from '@ember/object/computed';
import { get, computed, set } from '@ember/object';
import { inject as service } from '@ember/service';

export default Model.extend({
  session: service('session'),
  isAuthenticated: readOnly('session.isAuthenticated'),
  currentUser: currentUser(),

  user: belongsTo('user'),
  body: attr('string'),
  chat: belongsTo('chat'),
  createdAt: attr('date'),

  userAvatar: computed(
    'user',
    function() {
      if (!isEmpty(this.user)) {
        this.user.avatar.then((avatar) => {
          return  avatar;
        });
      }
    }),
});
