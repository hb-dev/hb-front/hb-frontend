import ToriiAuthenticator from 'ember-simple-auth/authenticators/torii';
import { inject as service } from '@ember/service';
import ENV from '../config/environment';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default ToriiAuthenticator.extend({
  torii: service(),
  ajax: service(),
  session: service(),
  notifications: service('notification-messages'),

  authenticate() {
    return this._super(...arguments)
      .then((data) => {
        const provider = data.provider;
        let tokenExchangeUri;
        if (provider === 'twitter') {
          tokenExchangeUri = ENV.torii.providers[provider].requestTokenUri;
        } else {
          tokenExchangeUri = ENV.torii.providers[provider].tokenExchangeUri;
          return this.ajax.request(tokenExchangeUri, {
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
              code: data.authorizationCode,
              provider,
            }),
          })
            .then((response) => {
              this.notifications.success(
                'Bienvenid@ a HelpButtons!',
                NOTIFICATION_OPTIONS,
              );
              return {
                email: response.email,
                token: response.authentication_token,
                provider,
              };
            })
            .catch(({ payload }) => {
              this.notifications.error(
                payload.errors[0].title,
                NOTIFICATION_OPTIONS,
              );
              this.notifications.error(
                payload.errors[0].detail,
                NOTIFICATION_OPTIONS,
              );
            });
        }
        return {
          email: data.currentUser.email,
          token: data.code,
          provider,
        };
      });
  },
});
