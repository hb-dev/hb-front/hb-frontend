import Base from 'ember-simple-auth/authenticators/base';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { get } from '@ember/object';
import { Promise } from 'rsvp';

export default Base.extend({
  ajax: service(),

  restore(data) {
    return new Promise((resolve, reject) => {
      if (!isEmpty(get(data, 'email')) && !isEmpty(get(data, 'token'))) {
        resolve(data);
      } else {
        reject();
      }
    });
  },
  authenticate(email, password) {
    return this.ajax.post('/users/sign_in', { data: { user: { email, password }}}).
      then(response => {
        return { email, token: response.authentication_token };
      });
  },

  invalidate() {
    return this.ajax.del('/users/sign_out');
  },
});
