import { configurable } from 'torii/configuration';
import Oauth2 from 'torii/providers/oauth2-code';

export default Oauth2.extend({
  name: 'instagram-oauth2',
  baseUrl: 'https://api.instagram.com/oauth/authorize/',

  // Additional url params that this provider requires
  requiredUrlParams: ['display'],

  responseParams: ['code', 'state'],

  scope: configurable('scope', 'basic'),

  display: 'popup',
  redirectUri: configurable('redirectUri', function() {
    // A hack that allows redirectUri to be configurable
    // but default to the superclass
    return this._super();
  }),

  open() {
    return this._super()
      .then(function(authData) {
        if (authData.authorizationCode && authData.authorizationCode === '200') {
          // indication that the user hit 'cancel', not 'ok'
          throw new Error('User canceled authorization');
        }

        return authData;
      });
  },

  fetch(data) {
    return data;
  },
});
