import TwitterProvider from 'torii/providers/twitter-oauth1';
import { reject } from 'rsvp';
import { inject as service } from '@ember/service';

export default TwitterProvider.extend({

  store: service(),

  fetch(data) {
    return data;
  },
  open: function() {
    const options = {};
    const context = this;
    const url = this.buildRequestTokenUrl();

    return this.get('popup').open(url, ['code', 'id', 'provider'], options)
      .then(function(authData) {
        if (!authData.code || !authData.id) {
          return reject();
        }

        localStorage.token = authData.code;
        const adapter = context.store.adapterFor('twitter');
        adapter.set('headers', { 'Authorization': localStorage.token });
        const code = authData.code;
        return context.get('store').find('user', authData.id)
          .then(function(user) {
            return {
              currentUser: user,
              code,
            };
          });
      });
  },
});
