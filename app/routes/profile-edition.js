import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { resolve, hash } from 'rsvp';
import { get } from '@ember/object';
import currentUser from 'help-button/utils/current-user';


export default Route.extend(
  AuthenticatedRouteMixin,
  {
    session: service(),
    currentUser: currentUser('session'),

    model() {
      let currentUserLet = this.session.currentUser;
      currentUserLet = currentUserLet.constructor.toString() === 'DS.PromiseObject' ?
        currentUserLet : resolve(currentUserLet);
      currentUserLet = this.currentUser;
      return currentUserLet.then((user) => {
        const avatar = get(user, 'avatar');
        const userTags = get(user, 'userTags');
        const selectedLanguages = get(user, 'languages');
        const languages = this.store.findAll('language');
        return hash({
          currentUserLet,
          avatar,
          userTags,
          languages,
          selectedLanguages,
        });
      });
    },
  }
);
