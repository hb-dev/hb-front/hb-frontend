import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  beforeModel() {
    this._super(...arguments);
    const button = this.modelFor('profile.new-button');
    if (isEmpty(button) || isEmpty(button.buttonType)) {
      this.transitionTo('profile.new-button');
    } else if (!button.hasValidTags || isEmpty(button.description)) {
      this.transitionTo('profile.new-button.description');
    } else if (isEmpty(button.latitude) || isEmpty(button.longitude)) {
      this.transitionTo('profile.new-button.location');
    }
  },
});
