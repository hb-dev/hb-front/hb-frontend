import Route from '@ember/routing/route';
import { set, get } from '@ember/object';

export default Route.extend({
  queryParams: {
    searchTags: {
      refreshModel: true,
      as: 'searchTags',
    },
  },

  controller: null,

  setupController(controller, model) {
    this._super(controller, model);
    set(this, 'controller', controller);
  },

  deactivate() {
    set(get(this, 'controller'), 'selectedButton', null);
    set(get(this, 'controller'), 'showProfile', null);
  },
});
