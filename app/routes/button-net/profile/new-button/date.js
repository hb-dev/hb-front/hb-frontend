import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  beforeModel() {
    this._super(...arguments);
    const button = this.modelFor('button-net.profile.new-button');
    if (isEmpty(button) || isEmpty(button.buttonType)) {
      this.transitionTo('button-net.profile.new-button');
    } else if (!button.hasValidTags || isEmpty(button.description)) {
      this.transitionTo('button-net.profile.new-button.description');
    } else if (isEmpty(button.latitude) || isEmpty(button.longitude)) {
      this.transitionTo('button-net.profile.new-button.location');
    }
  },
});
