import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { get, set } from '@ember/object';
import { resolve } from 'rsvp';

export default Route.extend(
  AuthenticatedRouteMixin,
  {
    session: service(),

    queryParams: {
      searchTags: {
        refreshModel: true,
        as: 'searchTags',
      },
    },

    buttonNetName: null,
    beforeModel(transition) {
      this._super(...arguments);
      set(this, 'buttonNetName', transition.params['button-net'].button_net_name);
    },
    setupController(controller, model) {
      this._super(controller, model);
      this.store.queryRecord('buttonNet', { name: get(this, 'buttonNetName')}).then((buttonNet) => {
        set(this, 'controller.buttonNet', buttonNet);
      });
    },

    model() {
      const currentUser = this.session.currentUser;

      document.title = 'Helpbuttons - Perfil de '+ currentUser.nickname;
      return currentUser.constructor.toString() === 'DS.PromiseObject' ?
        currentUser : resolve(currentUser);
    },

    deactivate() {
      set(this.controller, 'selectedButton', null);
      set(this.controller, 'showProfile', null);
    },
  }
);
