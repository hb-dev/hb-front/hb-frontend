import Route from '@ember/routing/route';
import { set, get } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  queryParams: {
    searchTags: {
      refreshModel: true,
      as: 'searchTags',
    },
  },

  controller: null,
  buttonNetName: null,

  beforeModel(transition) {
    this._super(...arguments);
    set(this, 'buttonNetName', transition.params['button-net'].button_net_name);
    // document.title = 'Helpbuttons - Perfil de '+ this.session.currentUser.nickname;

  },
  setupController(controller, model) {
    this._super(controller, model);
    set(this, 'controller', controller);
    this.store.queryRecord('buttonNet', { name: get(this, 'buttonNetName')}).then((buttonNet) => {
      set(this, 'controller.buttonNet', buttonNet);
    });
    if (isEmpty(this.session.currentUser)) {
      set(controller, 'accessDenied', '');
    } else {
      if (isEmpty(this.session.currentUser.internalId)) {
        get(this.session, 'currentUser').then((currentUser) => {
          if (get(currentUser, 'blockedBy').includes(Number(model.id))) {
            set(controller, 'accessDenied', 'disabled');
          } else {
            set(controller, 'accessDenied', '');
          }
        });
      } else {
        if (get(this.session.currentUser, 'blockedBy').includes(Number(model.id))) {
          set(controller, 'accessDenied', 'disabled');
        } else {
          set(controller, 'accessDenied', '');
        }
      }
    }


  },

  deactivate() {
    set(get(this, 'controller'), 'selectedButton', null);
    set(get(this, 'controller'), 'showProfile', null);
  },
});
