import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  beforeModel() {
    this._super(...arguments);
    const button = this.modelFor('button-net.buttons.new');
    if (isEmpty(button) || isEmpty(button.buttonType)) {
      this.transitionTo('button-net.buttons.new');
    } else if (!button.hasValidTags || isEmpty(button.description)) {
      this.transitionTo('button-net.buttons.new.description');
    } else if (isEmpty(button.latitude) || isEmpty(button.longitude)) {
      this.transitionTo('button-net.buttons.new.location');
    }
  },
});
