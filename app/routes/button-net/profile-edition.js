import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { resolve, hash } from 'rsvp';
import { get, set } from '@ember/object';
import currentUser from 'help-button/utils/current-user';


export default Route.extend(
  AuthenticatedRouteMixin,
  {
    session: service(),
    currentUser: currentUser('session'),

    buttonNetName: null,

    beforeModel(transition) {
      this._super(...arguments);
      set(this, 'buttonNetName', transition.params['button-net'].button_net_name);
    },
    setupController(controller, model) {
      this._super(controller, model);
      this.store.queryRecord('buttonNet', { name: get(this, 'buttonNetName')}).then((buttonNet) => {
        set(this, 'controller.buttonNet', buttonNet);
      });
    },

    model() {
      let currentUserLet = this.session.currentUser;
      currentUserLet = currentUserLet.constructor.toString() === 'DS.PromiseObject' ?
        currentUserLet : resolve(currentUserLet);
      currentUserLet = this.currentUser;
      return currentUserLet.then((user) => {
        const avatar = get(user, 'avatar');
        const userTags = get(user, 'userTags');
        const selectedLanguages = get(user, 'languages');
        const languages = this.store.findAll('language');
        return hash({
          currentUserLet,
          avatar,
          userTags,
          languages,
          selectedLanguages,
        });
      });
    },
  }
);
