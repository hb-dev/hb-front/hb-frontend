import Route from '@ember/routing/route';
import { get, set } from '@ember/object';

export default Route.extend({
  queryParams: {
    section: {
      as: 'section',
    },
  },

  buttonNetName: null,
  beforeModel(transition) {
    this._super(...arguments);
    set(this, 'buttonNetName', transition.params['button-net'].button_net_name);
  },
  setupController(controller, model) {
    this._super(controller, model);
    this.store.queryRecord('buttonNet', { name: get(this, 'buttonNetName')}).then((buttonNet) => {
      set(this, 'controller.buttonNet', buttonNet);
    });
  },
});
