// <Helpbuttons collaborative network making software>
//     Copyright (C) 2013-2121  Helpbuttons
//
//     This program is free software; you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation; either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License along
//     with this program; if not, write to the Free Software Foundation, Inc.,
//     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
import Mixin from '@ember/object/mixin';
import { get, getProperties } from '@ember/object';
import { inject as service } from '@ember/service';
import { hash, resolve } from 'rsvp';
import { isEmpty } from '@ember/utils';

export default Mixin.create({
  session: service(),
  modelRoute: '',

  actions: {
    willTransition(transition) {
      const button = this.modelFor(get(this, 'modelRoute'));

      if (get(this, 'session.isAuthenticated') && get(button, 'isNew')) {
        transition.abort();

        const { offerTags, neededTags } = getProperties(button, 'offerTags', 'neededTags');
        button.save()
          .then((savedButton) => {
            return hash({
              offerTags: isEmpty(offerTags) ? resolve() : savedButton.updateRelationship('offerTags'),
              neededTags: isEmpty(neededTags) ? resolve() : savedButton.updateRelationship('neededTags'),
            });
          })
          .then(() => {
            transition.retry();
          });
      }
    },
  },
});
